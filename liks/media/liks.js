﻿/*
 * liks.js - javascript/ecmascript for å regne liks 
 *           (lesbarhetsindeks)
 *
 * ========================================
 * (C) 2004,2009 Håvard Gulldahl, havard@lurtgjort.no
 *     CC-BY-SA 3.0
 * ----------------------------------------
 *
 * Sist endret: 2012-12-08
 *
 *
 * SLIK REGNER DU LIKS
 *
 * (se også liks.lurtgjort.no)
 *
 * a) tell antall ord i teksten
 * b) tell antall setninger (mellom to skilletegn: .?!)
 * c) tell antall lange ord (= ord med >=7 bokstaver)
 * d) beregn gjennomsnittlig setningslengde ( = a/b )
 * e) beregn andel langord i prosent ( = c/a * 100 )
 * f) liks = d + e, avrundet til n?rmeste heltall
 *
 *
 * GRADERING
 *    - 24 Lettlest (Barne og ungdomslitteratur)
 * 25 - 34 Enkel tekst (Ukeblader, skjønnlitteratur for voksne)
 * 35 - 44 Middels vanskelig (Aviser, magasiner, tidsskrifter)
 * 45 - 54 Vanskelig (Aviskommentarer, sakprosa)
 * 55 -    Svært vanskelig (faglitteratur)
 */

function Liks(s) {   
    //returnerer et objekt med mye interessant informasjon
    //og med to funksjoner: .beregn() og .forklar()
    var ord = filter_str(s.split(/[^A-Za-z0-9_-æøåöäÄÖÆØÅ]/), 1);
    //hjelp teksten litt
    // linje begynner med minus, en eller em dash => begynnelsen på setn.
    var ss = s.replace(/^(-|\u2013|\u2014)\s+/g, ". $1");  
    // hardt linjeskift => slutten på en setning
    ss = ss.replace(/\r\n/g, ". \r\n").replace(/\n/g, ". \n");
    var setninger = filter_str(ss.split(/[\.!\?]\s+/), 1);
    var liks = undefined;
    
    return {
      "tekst" : s,
      "ord" : ord,
      "setninger" : setninger,
      "lange_ord" : filter_str(ord, 7),
      "beregn" : function() {
        var antall_ord = ord.length
        var setningslengde = antall_ord / this.setninger.length;
        var lange_ord_andel = this.lange_ord.length / antall_ord * 100;
        liks = Math.round(setningslengde + lange_ord_andel);
        return { "antall_ord": antall_ord, 
                 "setningslengde": setningslengde,
                 "lange_ord": lange_ord_andel,
                 "liks": liks };
       },
      "forklar" : function() {
            // returnerer en tekstlig forklaring på liksen
           if(liks === undefined) {
              var stats = this.beregn();
              liks = stats.liks;
           }
           if(liks < 24) {
	        return "Lettlest (Barne og ungdomslitteratur)";
           } else if(liks <34) {
	        return "Enkel tekst (Ukeblader, skjønnlitteratur for voksne)";
           } else if(liks <44) {
	        return "Middels vanskelig (Aviser, magasiner, tidsskrifter)";
           } else if(liks <54) {
	        return "Vanskelig (Aviskommentarer, sakprosa)";
           } else {
	        return "Svært vanskelig (faglitteratur)";
           }
      }
    };
}

function filter_str(array, min_lengde)
{
    var ret = [];
    for(var z=0; z<array.length; z++) {
        if(array[z].length >= min_lengde)
            ret.push(array[z]);
    }
    return ret;
}


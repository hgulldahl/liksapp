from django.db import models

# Create your models here.
class savedText(models.Model):
  text = models.TextField("The full text")
  slug = models.SlugField("Short title")
  pubdate = models.DateTimeField("Date added", auto_now_add=True)
  origin = models.CharField("Origin of query", max_length=255, blank=True)
  referrer = models.CharField("Where the request came from", max_length=255, blank=True)
  def __unicode__(self): 
    return self.slug
  def excerpt(self):
    return self.text[:20]

class flag(models.Model):
  LEVEL_CHOICES = (
     ('E', 'Error'),
     ('W', 'Warning'),
     ('H', 'Hint'),
  )
  word = models.CharField("A word to look for", max_length=50)
  alternative = models.CharField(max_length=50, blank=True)
  details = models.TextField(blank=True)
  level = models.CharField(max_length=1, choices=LEVEL_CHOICES, default='W')
  source = models.CharField(max_length=255, blank=True)
  source_uri = models.CharField(max_length=255, blank=True)
  approved = models.BooleanField(default=0)
  def __unicode__(self):
    return self.word + u" -> " + self.alternative


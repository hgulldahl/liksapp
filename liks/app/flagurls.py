from django.conf.urls import url, include

import flagviews

urlpatterns = [
    url(r'^(?P<flag_id>\d+)(?P<use_json>.json)?$', flagviews.flagGet),
    url(r'^(?P<word>\w+)(?P<use_json>.json)?$', flagviews.flagGet),
    url(r'^:$', flagviews.flagScan),
    url(r'^\+$', flagviews.flagPut),
]

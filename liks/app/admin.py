from liks.app.models import savedText, flag
from django.contrib import admin

class savedTextAdmin(admin.ModelAdmin):
  prepopulated_fields = { 'slug' : ( 'text', ) }
  fields = ['slug', 'text', ]
  list_display = ('id', 'slug', 'excerpt', 'pubdate', 'origin', 'referrer')
  list_filter = ['pubdate']
  date_hierarchy = 'pubdate'
  search_fields = ['text', 'referrer']

class flagAdmin(admin.ModelAdmin):
  search_fields = ('word', 'source')


admin.site.register(savedText, savedTextAdmin)
admin.site.register(flag, flagAdmin)

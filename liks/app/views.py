# Create your views here.
# -*- coding: utf-8 -*-

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render_to_response, get_object_or_404
import requests
from readability import Document
import html2text

def index(request):
  return render_to_response("index.html", {})


def get_url(request, url):
  'Get url for analysis'
  response = requests.get(url)
  doc = Document(response.text)
  h = html2text.HTML2Text()
  h.ignore_links = True
  h.ignore_images = True
  h.ignore_emphasis = True
  h.ignore_tables = True
  h.decode_errors = 'ignore'
  h.skip_internal_links = True
  h.unicode_snob = True
  h.emphasis_mark = ''
  h.strong_mark = ''
  text = h.handle(doc.summary())
  return HttpResponse(text, content_type='text/plain;charset=utf-8')

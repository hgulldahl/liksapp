# Create your views here.
# -*- coding: utf-8 -*-

import re

from django.http import HttpResponse, HttpResponseNotAllowed, HttpResponseBadRequest
from django.shortcuts import render_to_response, get_object_or_404
from liks.app.models import savedText

def textPut(request):
  if request.META["REQUEST_METHOD"].upper() not in ("PUT", ):
    return HttpResponseNotAllowed(['PUT',])
  if request.META["CONTENT_TYPE"].lower() not in ("text/plain", ):
    return HttpResponseBadRequest("You may only add plain texts")

  t = request.raw_post_data.strip()[5120:] # limit 5k
  try:
    _slug = re.compile(r"[^0-9a-z_-]").sub("", t[:50].lower().replace(" ", "-"))
  except:
    _slug = ""
  try:
    _origin = request.META["ORIGIN"]
  except KeyError:
    _origin = ""
  try:
    _referrer = request.META["REFERER"]
  except KeyError:
    try:
      _referrer = request.META["REFERRER"]
    except KeyError:
      _referrer = ""
  txt = savedText(text=t, slug=_slug, origin=_origin, referrer=_referrer)
  txt.save()
  return HttpResponse("http://liks.lurtgjort.no/tekst/%s+%s" % (txt.id, txt.slug))

def textGet(request, text_id, text_slug=None):
  txt = get_object_or_404(savedText, pk=text_id)
  return render_to_response("index.html", { 'tekst' : txt })

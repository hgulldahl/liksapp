# Create your views here.
# -*- coding: utf-8 -*-

import re

from django.core import serializers
from django.http import HttpResponse, HttpResponseRedirect, \
    HttpResponseBadRequest, HttpResponseNotAllowed, HttpResponseNotFound
from django.shortcuts import render_to_response, get_object_or_404
from liks.app.models import flag

reW = re.compile(r"\W", re.UNICODE)

def flagScan(request):
  try:
    str = request.POST["string"].strip()
  except KeyError:
    return HttpResponseBadRequest("Missing required field 'string'")
  words = list(set(("'%s'" % s for s in reW.split(str) if len(s) > 0)))
  flags = flag.objects.extra(where=["word IN (%s)" % ",".join(words),])
  #return render_to_response("scan.json", { 'flags' : flags })
  r = HttpResponse(content_type="text/javascript;charset=utf8")
  json_serializer = serializers.get_serializer("json")()
  json_serializer.serialize(flags, ensure_ascii=False, stream=r)
  return r

def flagPut(request):
  print request.POST
  if request.META["REQUEST_METHOD"].upper() not in ("POST", ):
    return HttpResponseNotAllowed(['POST',])
  try:
    f = flag(word=request.POST["word"].strip(), 
             level=request.POST["level"].strip())
  except KeyError:
    return HttpResponseBadRequest("Missing required field")
  for z in ("alternative", "source", "source_uri", "details"):
    try:
      setattr(f, z, request.POST[z].strip())
    except:
      pass
  f.save()
  r = HttpResponse("http://liks.lurtgjort.no/ord/%s" % f.id, status=201)
  return r

def flagGet(request, flag_id=None, word=None, use_json=None):
  if flag_id is not None:
    f = get_object_or_404(flag, pk=flag_id)
  elif word is not None:
    f = get_object_or_404(flag, word=word)
  else:
    return HttpResponseNotFound()
  r = HttpResponse(content_type="text/javascript;charset=utf8")
  json_serializer = serializers.get_serializer("json")()
  json_serializer.serialize([f,], ensure_ascii=False, stream=r)
  return r


from django.conf.urls import url, include

import textviews

urlpatterns = [
    url(r'^$', textviews.textPut),
    url(r'^(?P<text_id>\d+)/?$', textviews.textGet),
    url(r'^(?P<text_id>\d+)(\+|\ |%20)(?P<text_slug>[-_a-z0-9]+)?/?$', textviews.textGet),
]

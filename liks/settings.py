# Django settings for liks project.
#-*- encoding: utf-8 -*-

import os.path, sys
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

DEVELOPMENT_MODE = os.path.exists(BASE_DIR + '/_DEVELOPMENT_MODE')
DEBUG = os.path.exists(BASE_DIR + '/_DEBUG')

ADMINS = (
   (u'Håvard Gulldahl', 'havard@gulldahl.no'),
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'lurtgjort_liks',
        'USER': 'lurtgjort',
        'PASSWORD': 'GLVngZUnT2vN',
        'HOST': 'db14.subsys.no',
        'PORT': '5432',
    }, 
    'develop': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR+'/liks.db'
    }
}

if DEVELOPMENT_MODE:
    DATABASES["default"] = DATABASES["develop"]

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Oslo'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
STATICFILES_DIRS = [BASE_DIR + '/media/','/home/lurtgjort/var/liks/media/']

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
STATIC_URL = '/media/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/admin_media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'ri7o)!t(%-mi(0q)7l==7&5hahw_7u5-)bo1-%(61o^h(pye42'

MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
)

ROOT_URLCONF = 'liks.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            BASE_DIR + "/templates",
            "/home/lurtgjort/var/liks/templates",
            # insert your TEMPLATE_DIRS here
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.staticfiles',
    'liks.app',
)

ALLOWED_HOSTS = ['liks.lurtgjort.no', '127.0.0.1']

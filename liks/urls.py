from django.conf import settings
from django.conf.urls import url, include

from liks.app import views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    # Example:
    url(r'^$', views.index),
    url(r'^tekst/', include('liks.app.texturls')),
    url(r'^ord/', include('liks.app.flagurls')),
    url(r'^url/(?P<url>.*)', views.get_url),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
]

#if settings.DEVELOPMENT_MODE:
  #urlpatterns += [
    #url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
      #{'document_root': settings.MEDIA_ROOT}),
  #]
